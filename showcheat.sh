#!/usr/bin/env bash

# Hans Stenglein, Jan. 2023
# Licensed under the EUPL

show() {
  # show cheatsheet file $1
  echo # newline
  # ? a header line looked nice first, but got messy
  #NAME="$( echo "$1" | sed 's/[a-z]/\U&/g' )"
  #HEADER="\e[1;32m#######  $NAME CHEATSHEET #######\e[0m"
  $CATMD "$1".md
}

help() {
  PROG=$0 # cheat
  # display help
  echo "usage:"
  echo -e "\t$PROG --help \t :\tshow this help"
  echo -e "\t$PROG \t\t :\tselect a cheatsheet to show"
  echo -e "\t$PROG <cs> \t\t :\tshow cheatsheet <cs>"
  echo -e "\t$PROG <cs> <ln>\t :\texecute line <ln> from <cn>"
}
if [ "$1" == "--help" ] ; then
  help
  exit 0
fi

# jal
PWD=$(pwd)
cd ~/.bash/cheatsheet || exit 1 # IMPROVE abs path unix
success() {
  # restore PS3 ?
  cd "$PWD" || exit 3
  exit 0
}
# find cat like = CATMD
for B in bat cat; do # LIST cats
  which "$B" >> /dev/null && CATMD=$B; break
done  

# --- setup done


# List of available cheat sheets
#        ls dir | as lines          | md only    | drop README, ".md" + "./" from find
CHEATS=$(find . | sed 's/\ \ /\n/g' | grep ".md" | grep -v "README.md" | sed 's/.md//g' | sed 's/.\///g')

# show cheatsheet given as arg if any, execute line from cheatsheet if any
# cheat <cs> <ln>
for C in $CHEATS; do
  if [ "$1" == "$C" ] ; then    
  # arg is valid cheatsheet
      if [ -n "$2" ] && [ "$2" -ge 0 ] ; then
      # second arg is a line >> exec line from cheatsheet 
      # TODO this is unsave??!!
          # cut command from file
          X="$( head -"$2" "$C".md | tail -1 )"
          if command -v $X > /dev/null
          then
          # is executable, run the line, report non-0
            ( $X ) 2> /dev/null || echo -e "$X ; \e[0;33m(empty output?) returned\e[0;31m $?\e[0m"
            success
          else
          # no code line?
            ( echo -e "\e[0;31m> $1.md:$2 can't be executed - no command but plaintext?:\e[0m"
            echo -e "\t\e[0;32m$X\e[0m" )
            success
          fi
      else
      # just show cheatsheet
          show "$1"
          success
      fi
  fi
done

# warn that <cs> is unknown, proceed with list
if [[ -n "$1" ]] ; then
    echo -e "\e[0;33munknown cheatsheet <$1>\e[0m"
    echo 
fi

# list, ask which cheatsheet to show
echo "available cheatsheets:"
PS3="select cheatsheet #: " # prompt select
select C in $CHEATS; do 
  if [[ -z $C ]] # invalid = dont show
  then
      echo "dont show cheatsheet"
      success 
  fi

  show "$C" # show selected cheatsheet  
  success
done