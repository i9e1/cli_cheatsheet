# Cheatsheet for package management on arch

Default package manager is `pacman`. 
Using *unofficial* packages from the *AUR* is easier with an AUR-helper like `yay`.

## Cleanup procedure

# Single package

* Search installed packages (for a package of interest)
* View the infos for the specific package
* Show which packages depend on
* (Show the dependency tree of the specific package)

Remove the package (and its than unneeded dependencies) if you want.

    yay -Qs <search-term>
    yay -Qi <pkg>
    pactree -r <pkg>
    pactree <pkg>

    yay -Rs <pkg>


## all ophaned packages

List ophaned packages (installed `--asdeps`, but not linked to a dependency anymore)
and use list to remove

    yay -Qdtq
        
    yay -Rs $(yay -Qdtq)
or

    yay -Qdtq | yay -Rns -


## dependency cycles

    yay -Qqd | yay -Rsu --print -

