# Cheatsheet for power management, battery etc

## acpi

Short info for battery and AC-adapter only.

    acpi -abi

Show info for each power related device status (battery, thermal sensors, cooling, ...).

    acpi -V


## UPower

UPower shows a lot of stats.
 
First, list all power sources.
Then use the `/org/freedesktop/...` path to show 
the stats of a specific device.

    upower -e
    upower -i <path_freedesktop>


## digging in /sys

You may find additional infos in `sys/class`
subdirectories, sorted into categories and *devices*.

    cd /sysy/class/power_supply/BAT0
    ls
    cat energy_full
    cat energy_design
    ...
