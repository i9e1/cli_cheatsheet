# cheatsheets, tools for CLI usecases

*Spicker* to avoid RTFM + online search for useful, but unfrequent tasks.

The cheatsheets are written in a short reference style with minimal explanation
(you know what to do, but not the exact tool or arguments).
This project is basically a collection of short Markdown files
and a bash script listing and dumping them.

## Usage

You may clone the repo and define an `alias` pointing to `some/path/showcheats.sh`.

There are three modes. 

1. Without any argument, you are asked which 
cheatsheet you want to see. 
2. You may pass a cheatsheet name directly, skipping the selection step (without `.md` suffix). 
3. When you pass a cheatsheet name and a number, 
the script tries to execute the corresponding line in the cheatsheet. 
This feature is intended to be called after reading the cheatsheet first. 
THIS IS EXPERIMENTAL AND POTENTIALLY UNSAFE.

A call with `--help` lists possible usage:

    $ cheat --help
    
    usage:
	    cheat --help    : show this help
	    cheat           : select a cheatsheet to show
	    cheat <cs>      : show cheatsheet <cs>
	    cheat <cs> <ln> : execute line <ln> from <cn>


## Contributing

Feel free to suggest changes and additions.
If your tool or usecase is missing, just add it as PR.

(With only a handful of cheatsheets selecting via `bash select` is fine.)
