# Cheatsheet for memory check and management on unix

## See current mem + swap useage

simplest:

    free -h

Tabellarische Übersicht mit (h)top

    top
    htop

* Sortierung nach mem usage: ``M``

active swap devices

    swapon --show=NAME,SIZE,USED,PRIO
